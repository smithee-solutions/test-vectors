# copyright stuff

VERSION=0.0.0-2

all:	build

clean:
	(cd test-jigs; make clean)

build:
	mkdir -p samples
	echo >samples/samples.log test-vectors ${VERSION} is in start-up.
	(cd test-jigs; make build)

